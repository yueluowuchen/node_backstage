const express = require('express');
const router = express.Router();
const MySQL = require('../../mysql/mysql');
const Log = require('../../common/log');
const bodyParser = require('body-parser');
const MD5 = require('md5');
const Common = require("../../common/common");
const uuidv1 = require('uuid/v1');
const ResList = require('../../common/responseList');
const RemoveRepeat = require('../../common/removerepeat');

// 配置body-parser
router.use(bodyParser.urlencoded({
	extended: true
}));
router.use(bodyParser.json());

/**
 * 用户注册 接收昵称 用户名 用户密码
 */
router.post('/registered.node', (req, res) => {
	/**
	 * 接受客户端注册信息
	 * 返回注册成功与否
	 */
	// 获取用户从客户端传递过来的数据
	const reqdata = {
		UserName: req.body.UserName,
		PassWord: MD5(req.body.PassWord),
		NickName: req.body.NickName,
		RequestToken:uuidv1()
	}

	// 检验客户端传过来的数据是否为空 防止遇到数据库必填字段造成程序崩溃
	if (reqdata.UserName && reqdata.PassWord && reqdata.NickName) {
		// 数据库验重 传入表名、验重字段 接收查询信息
		RemoveRepeat.RemoveRepeat('t_user', reqdata.UserName, (data) => {
			// 不为0 说明存在重复数据
			if (data.length != 0) {
				// 响应客户端
				ResList.ResList(req, res, '-1', '用户名已存在', {});
				// 记录日志
				Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '-1', '用户名已存在',reqdata.RequestToken);

				return;
			} else { //反之没有重复数据，可以进行插入
				// 执行插入操作
				MySQL.Mysql('INSERT INTO t_user SET ?', reqdata, (err, results, fields) => {
					if (err) {
						throw err;
						// 响应客户端
						ResList.ResList(req, res, '1', '注册失败', {});
						// 记录日志
						Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '1', '注册失败',reqdata.RequestToken);
					} else {
						// 响应客户端
						ResList.ResList(req, res, '0', '注册成功', {});
						// 记录日志
						Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '0', '注册成功',reqdata.RequestToken);
					}
				})
			}
		});
	} else {
		// 响应客户端
		ResList.ResList(req, res, '-2', '空指针异常', {});
		// 记录日志
		Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '-2', '空指针异常',reqdata.RequestToken);
	}
})

module.exports = router;