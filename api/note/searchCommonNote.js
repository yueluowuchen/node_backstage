const express = require("express");
const router = express.Router();
const MySQL = require("../../mysql/mysql");
const Log = require("../../common/log");
const bodyParser = require("body-parser");
const Common = require("../../common/common");
const ResList = require("../../common/responseList");

// 配置body-parser
router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

/**
 * 查询公有笔记
 * 服务器通过数据库筛选出isSelf不等于1的返回给客户端
 */

 router.post('/searchCommonNote.node',(req,res)=>{
    // 进行查询数据
    let searchsql = "select s_note.*,t_user.NickName from s_note inner join t_user on t_user.RequestToken = s_note.UserToken where IsSelf = 0";        
    MySQL.Mysql(searchsql,{},(err,results)=>{
        if(err){
            throw err;
            // 响应客户端
            ResList.ResList(req, res, '1', '查询失败','');
            // 记录日志
            Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '1', '查询失败',"");
        }else{
            // 响应客户端
            ResList.ResList(req, res, '0', '查询成功', results);
            // 记录日志
            Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '0', '查询成功',''); 
        }
    })
 })

module.exports = router;