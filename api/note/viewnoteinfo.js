const express = require("express");
const router = express.Router();
const MySQL = require("../../mysql/mysql");
const Log = require("../../common/log");
const bodyParser = require("body-parser");
const Common = require("../../common/common");
const ResList = require("../../common/responseList");

// 配置body-parser
router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

/**
 * 查看笔记详情
 * 客户端传入笔记ID
 * 服务器返回笔记的所有信息
 */

 router.post('/viewnoteinfo.node',(req,res)=>{
    //  接收客户端请求的数据
    let viewdata = {
        ID:req.body.ID
    }
    if(viewdata.ID){
        let searchsql = "select s_note.*,t_user.NickName from s_note left join t_user on t_user.RequestToken = s_note.UserToken where s_note.ID =" + MySQL.mysql.escape(viewdata.ID);
        MySQL.Mysql(searchsql,{},(err,results)=>{
            if(err){
                throw err;
                // 响应客户端
				ResList.ResList(req, res, '1', '查询失败', {});
				// 记录日志
				Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '1', '查询失败','');
            }else{
                // 响应客户端
                ResList.ResList(req, res, '0', '查询成功', results);
                // 记录日志
                Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '0', '查询成功','');
            }
        })
    }else{
        // 响应客户端
        ResList.ResList(req, res, '-2', '空指针异常', {});
        // 记录日志
        Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '-2', '空指针异常','');
    }
 })

module.exports = router;