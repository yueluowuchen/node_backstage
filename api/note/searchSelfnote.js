const express = require("express");
const router = express.Router();
const MySQL = require("../../mysql/mysql");
const Log = require("../../common/log");
const bodyParser = require("body-parser");
const Common = require("../../common/common");
const ResList = require("../../common/responseList");

// 配置body-parser
router.use(
  bodyParser.urlencoded({
    extended: true
  })
);
router.use(bodyParser.json());

/**
 * 查询笔记
 * 接收客户端传入的UserToken,isSelf(判断笔记的公有私有)
 * 服务器返回查询到的结果
 */

 router.post('/searchSelfNote.node',(req,res)=>{
    // 接收客户端的数据
    let searchdata = {
        UserToken:req.body.UserToken
    } 
    
    if(searchdata.UserToken){
        // 进行查询数据
        let searchsql = "select s_note.*,t_user.NickName from s_note inner join t_user on t_user.RequestToken = s_note.UserToken where UserToken = " + MySQL.mysql.escape(searchdata.UserToken);        
        MySQL.Mysql(searchsql,{},(err,results)=>{
            if(err){
                throw err;
                // 响应客户端
				ResList.ResList(req, res, '1', '查询失败',{});
				// 记录日志
				Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '1', '查询失败',searchdata.UserToken);
            }else{
                // 响应客户端
				ResList.ResList(req, res, '0', '查询成功', results);
				// 记录日志
				Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '0', '查询成功',searchdata.UserToken); 
            }
        })

    }else{
    // 响应客户端
    ResList.ResList(req, res, '-2', '空指针异常', {});
    if(searchdata.UserToken){
        // 记录日志
	    Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '-2', '空指针异常',searchdata.UserToken);
    }else{
        // 记录日志
	    Log.Log4j(Common.getClientIp(req).substring(7), Common.dateFormat(), req.path.substring(1), '-2', '空指针异常','');
    }
    }
    
 })

module.exports = router;