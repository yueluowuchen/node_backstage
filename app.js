const express = require('express');
const app = express();
const responseTime = require('response-time');
const Common = require('./common/common');
const routes = require('./routers/index');
const Util = require('./routers/util/wechat');
const engines = require('consolidate');
const corss = require('./common/crossdoman');

// 处理跨域请求
corss.CrossDoman(app);

/*配置html模板引擎*/
app.engine('html', engines.mustache);
app.set("view engine", "html");
app.use(express.static("public"));

// 设置views下的index.html为页面主入口
app.get('/', (req, res) => {
    res.render('index');
})

/**
 * 设置/routes/index文件为总的路由控制文件
 * 在routes/index文件中再进行统一的路由分发，这样防止app.js中代码过于臃肿
 */
// 页面逻辑访问
routes(app);
// 工具接口访问
Util(app);
// 启动服务监听
const server = app.listen(80, () => {
    let host = server.address().address;
    let port = server.address().port;
    console.log('Example app listening at http://%s:%s', host, port);
})