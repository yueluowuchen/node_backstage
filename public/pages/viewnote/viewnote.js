$(()=>{
    // 获取笔记ID和对应的用户UserToken
    var NoteID = GetIframeQueryString('noteid');
    var UserToken = GetIframeQueryString('usertoken');

    // 根据查询的内容填入页面
    function appenDom(NickName,NoteTitle,NoteContent){        
        // 作者昵称
        if(NickName){
            $('#NickName').html(NickName);
        }else{
            $('#NickName').html('无');
        }
        // 笔记标题
        if(NoteTitle){
            $('#NoteTitle').html(NoteTitle);
        }else{
            $('#NoteTitle').html('无');
        }
        // 笔记内容
        if(NoteContent){
            $('#notecontent').html(NoteContent);
        }else{
            $('#notecontent').html('无');
        }
    }
    
    // 根据传入的ID获取笔记的详情
    function viewnoteinfo(NoteID){
        if(NoteID){
            $.post('/viewnoteinfo/viewnoteinfo.node',{ID:NoteID},(data)=>{
                if(data.status == 0){                    
                    appenDom(data.data[0].NickName,data.data[0].NoteTitle,data.data[0].NoteContent)
                }
            })
        }
    }
    viewnoteinfo(NoteID);
})