$(() => {
  // 处理编辑器
  $("#md").on("keyup blur", function() {
    $("#html").html(marked($("#md").val()));
  });

  // 处理文档是否为私有
  var isSelf = true;
  $("#isSelf").click(() => {
    if (isSelf) {
      $("#isSelf").addClass("isOpen");
      isSelf = false;
    } else {
      $("#isSelf").removeClass("isOpen");
      isSelf = true;
    }
  });
  
  // $('#save').click(()=>{
  //   console.log(isSelf);
  // })

  // 清理用户输入区域
  function clearform(){
    $('#title').val('');
    $('#html').html('');
    $('#md').val('');
  }

  // 保存用户编辑的内容
  $('#save').click(()=>{
    // 保存用户输入的内容
    let saveData = {
      UserToken:localStorage.getItem('UserToken'),
      NoteTitle:$('#title').val(),
      NoteContent:$('#html').html(),
      isSelf:Number(isSelf)
    }
    if(saveData.NoteTitle == ''){
      waringinfo('submitinfo','请输入笔记名称');
      return;
    }
    $.post('/createnote/createnote.node',saveData,(data)=>{
      if(data.status == 0){
        clearform();
        waringinfo('submitinfo',data.msg);
        window.frameElement.src = 'pages/main/main.html';
      }else{
        waringinfo('submitinfo',data.msg);
      }
    })
  })
  
});
