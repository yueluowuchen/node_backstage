$(() => {
    var isSelf = true;

     // 选中状态
    //  个人笔记
    $('#selfNote').click(()=>{
        $('#cncenter').empty();
        $('#selfNote').addClass('selectNote');
        $('#commonNote').removeClass('selectNote');
        // 请求后台
        queryPost(true,(data)=>{
            forDom(data.data);
        })
    })
     // 请求后台
     queryPost(isSelf,(data)=>{
        forDom(data.data);
    })

    // 公共笔记
    $('#commonNote').click(()=>{
        $('#cncenter').empty();
        $('#commonNote').addClass('selectNote');
        $('#selfNote').removeClass('selectNote');
        // 请求后台
        queryCommon((data)=>{
            forDom(data.data);
        })
    })

    //获取当前用户的所有笔记
    function queryPost(isSelf,callback){
        let searchdata = {
            UserToken:localStorage.getItem('UserToken'),
        }
        if(searchdata.UserToken==''){
            console.log('该用户无法获取');
            return;
        }
        $.post('/searchSelfNote/searchSelfNote.node',searchdata,(data)=>{
            if(data.status == 0){
                callback(data);
            }
        })
    }

    // 获取公共笔记
    function queryCommon(callback){
        $.post('/searchCommonNote/searchCommonNote.node',{},(data)=>{
            if(data.status == 0){
                callback(data);
            }
        })
    }

    function addChild(nicName,title,time,isCommon,id,usertoken){
        var child;
        if(isCommon){
            child = "<div class='notearea'><a href='#'  class='gonote' id='goinfo'><input type='hidden' value="+id+"></input><input type='hidden' value="+usertoken+"></input><span class='nickname'><i id = 'iCon' class='fa fa-user'></i>"+nicName+"</span><span class='title'>"+title+"</span> <span>"+time+"</span></a></div>";
        }else{
            child = "<div class='notearea'><a href='#'  class='gonote' id='goinfo'><input type='hidden' value="+id+"></input><input type='hidden' value="+usertoken+"></input><span class='nickname'><i id = 'iCon' class='fa fa-users'></i>"+nicName+"</span><span class='title'>"+title+"</span> <span>"+time+"</span></a></div>";
        }                
        $('#cncenter').append(child);
    }

    function forDom(arr){
        for(var i = 0; i<arr.length;i++){            
            var nicName = arr[i].NickName;
            var title = arr[i].NoteTitle;
            var time = arr[i].CreateTime;
            var isCommon = arr[i].IsSelf;
            var id = arr[i].ID;
            var usertoken = arr[i].UserToken;
            addChild(nicName,title,time,isCommon,id,usertoken);
        }
    }

   
    // 创建笔记
    $('#user_create').click(()=>{
        window.frameElement.src = 'pages/createnote/createnote.html';
    })
    
    // 为生成的dom添加点击事件
    $(document).on("click", ".gonote", function () {
        let noteid = this.children[0].defaultValue;
        let usertoken = this.children[1].defaultValue;
        window.frameElement.src = 'pages/viewnote/viewnote.html?noteid='+noteid+'&usertoken='+usertoken;
    })
});

// function govienote(id,usertoken){
//     console.log(id,usertoken);
// }
