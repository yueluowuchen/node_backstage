$(() => {
    // clearform
    function clearform() {
        $('#loginname').val('');
        $('#password').val('');
    }
    // 执行登录
    $('#submit').click(() => {
        // 获取用户的登录数据
        var logindata = {
            UserName: $('#loginname').val(),
            PassWord: $('#password').val(),
        }

        // 验证用户操作
        if (logindata.UserName == '') {
            waringinfo('loginnameinfo', '请输入用户名');
            return;
        }

        if (logindata.PassWord == '') {
            waringinfo('passwordinfo', '请输入密码');
            return;
        }
        $.post('/login/login.node', logindata, (data) => {
            if (data.status == 0) {
                clearform();
                // 登录成功进行父级页面刷新
                parent.location.reload();
                // 把返回的登录信息存入localstorage
                var localArray = ['UserName', 'PassWord', 'NickName','UserToken'];
                var userinfo = {
                    UserName: data.data[0].UserName,
                    PassWord: data.data[0].PassWord,
                    NickName: data.data[0].NickName,
                    UserToken:data.data[0].RequestToken
                }
                localArray.forEach(v => {
                    localStorage[v] = userinfo[v];
                })
                waringinfo('submitinfo', data.msg);
            } else {
                clearform();
                waringinfo('submitinfo', data.msg);
            }
        })
    })

    // 去注册
    $('#registered').click(() => {
        window.frameElement.src = 'pages/registered/registered.html';
    })
})