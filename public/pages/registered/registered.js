$(() => {
    // formclear
    function clearform() {
        $('#loginname').val('');
        $('#nickname').val('');
        $('#password').val('');
        $('#confirmpassword').val('');
    }
    // 注册按钮
    $('#submit').click(() => {
        // 获取用户输入的注册信息
        let registerdata = {
            UserName: $('#loginname').val(),
            NickName: $('#nickname').val(),
            PassWord: $('#password').val()
        }

        // 验证用户是否输入用户名
        if (registerdata.UserName == '') {
            waringinfo('loginnameinfo', '请输入用户名');
            return;
        }
        // 验证用户输入的用户名是否是英文数字或者汉字 
        if (!inputlogin(registerdata.UserName)) {
            waringinfo('loginnameinfo', '用户名只能包含数字英文和中文');
            return;
        }
        // 长度是否超过10位
        if (registerdata.UserName.length > 10) {
            waringinfo('loginnameinfo', '用户名长度不能超过10');
            return;
        }

        // 验证昵称
        if (registerdata.NickName == '') {
            waringinfo('nicknameinfo', '请输入用户昵称');
            return;
        }
        // 验证用户输入的昵称是否是英文数字或者汉字 
        if (!inputlogin(registerdata.UserName)) {
            waringinfo('loginnameinfo', '昵称只能包含数字英文和中文');
            return;
        }
        // 长度是否超过10位
        if (registerdata.NickName.length > 5) {
            waringinfo('loginnameinfo', '用户昵称长度不能超过5');
            return;
        }

        // 验证密码
        if (registerdata.PassWord == '') {
            waringinfo('passwordinfo', '请输入用户密码');
            return;
        }

        if (registerdata.PassWord.length > 18) {
            waringinfo('passwordinfo', '用户密码长度不能超过18位');
            return;
        }

        // 验证确认密码
        if ($('#confirmpassword').val() == '') {
            waringinfo('confriminfo', '请输入确认密码');
            return;
        }

        if ($('#confirmpassword').val() != registerdata.PassWord) {
            waringinfo('confriminfo', '两次输入的密码不相同');
            return;
        }

        // 执行注册
        $.post('/registered/registered.node', registerdata, (data) => {
            if (data.status == 0) {
                clearform();
                waringinfo('submitinfo', data.msg);
                window.frameElement.src = 'pages/login/login.html';
            } else {
                clearform();
                waringinfo('submitinfo', data.msg);
            }
        })
    })

    // 已有帐号执行登录
    $('#login').click(() => {
        window.frameElement.src = 'pages/login/login.html';
    })
})