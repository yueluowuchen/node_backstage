$(function () {
  // 点击注册ifream加载注册页面
  $("#registed").click(function () {
    $(".ifream").attr({
      src: "pages/registered/registered.html"
    });
  });

  // 点击登录ifream加载注册页面
  $("#login").click(function () {
    $(".ifream").attr({
      src: "pages/login/login.html"
    });
  });

  // 页面加载完成查看localstorage 并进行一次模拟登录
  function cheackLogin() {
    var logindata = {
      UserName: localStorage.getItem("UserName"),
      PassWord: localStorage.getItem("PassWord")
    };

    if (logindata.UserName && logindata.PassWord) {
      $("#loginout").addClass("hide");
      $("#logining").removeClass("hide");
      $(".ifream").attr({
        src: "pages/main/main.html"
      });
    } else {
      $(".ifream").attr({
        src: "pages/index/index.html"
      });
    }
  }
  cheackLogin();

  //用户登出
  $('#user_out').click(() => {
    localStorage.clear();
    window.location.reload();
  })


  /**
   * 微信测试
   */
  $('#ceshi').click(() => {
    $.get('/weixin/wxJSSDK/getJssdk.node', {
      url: location.href
    }, (data) => {
      console.log(data);
      wx.config({
        debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
        appId: data.data.appId, // 必填，公众号的唯一标识
        timestamp: data.data.timestamp, // 必填，生成签名的时间戳
        nonceStr: data.data.nonceStr, // 必填，生成签名的随机串
        signature: data.data.signature, // 必填，签名，见附录1
        jsApiList: ['onMenuShareTimeline', 'onMenuShareAppMessage'] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
      });

      wx.ready(function () {
        wx.onMenuShareTimeline({
          title: wxShare.title,
          desc: wxShare.desc,
          link: wxShare.link,
          imgUrl: wxShare.imgUrl
        });

        wx.onMenuShareAppMessage({
          title: wxShare.title,
          desc: wxShare.desc,
          link: wxShare.link,
          imgUrl: wxShare.imgUrl
        });
      })

      wx.error(function (res) {
        // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
      })
    })
    
  })


});