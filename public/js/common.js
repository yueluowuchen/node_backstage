//用户警示提示信息
function waringinfo(id, msg) {
  $("#" + id).html(msg);
  $("#" + id).css({
    display: "block"
  });
  setTimeout(() => {
    $("#" + id).css({
      display: "none"
    });
  }, 2000);
}

// 验证用户名 只能输入数字英文和汉字
function inputlogin(value) {
  var regx = /^[\u4E00-\u9FA5A-Za-z0-9]+$/;
  if (regx.test(value)) {
    return true;
  } else {
    return false;
  }
}

// ifrea页面跳转的参数获取
function GetIframeQueryString(name) {
  // var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
  // console.log(window.frameElement.src);
  
  // var r = window.frameElement.src.substr(1).match(reg);
  // console.log(r);
  
  // if (r != null) {
  //   return decodeURI(r[2]);
  // }
  // return null;
  var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"); 
        var r = window.location.search.substr(1).match(reg); 
        if (r != null) return unescape(r[2]); 
        return null; 
}
