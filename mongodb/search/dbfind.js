const connectDb = require('../mongos');
// 数据库查询
exports.find = (collection,searchdata,callback)=>{
    connectDb((db,client)=>{
        var result=db.collection(collection).find(searchdata);
        result.toArray(function(error,data){
            callback(error,data);
            client.close();
        })
    })
}   