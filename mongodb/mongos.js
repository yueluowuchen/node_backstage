const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');
/**
 * 
 * 目前MonoDB存在请求速度延迟问题！！！
 */
// 引入数据库地址配置文件
const MogodbCofig = require('../config/mogodburl')
//设备数据库地址
const url = MogodbCofig.url;
// 数据库名称
const dbName = MogodbCofig.database;

// 链接数据库
function _connectDb(callback) {
    MongoClient.connect(url, function (err, client) {
        assert.equal(null, err);
        console.log("Connected successfully to server");
        const db = client.db(dbName);
        callback(db,client);
    });
}

module.exports = _connectDb;