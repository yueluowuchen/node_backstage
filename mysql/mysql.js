var mysql = require('mysql');
const MySqlConfig = require('../config/mysqlurl');

exports.Mysql = (sqlorder, post, callback) => {
    // 数据库连接配置应该写在里面，否则会让node失去对数据库的连接
    /**
     * 接收sql语句和执行条件
     * 返回执行结果
     */
    var connection = mysql.createConnection({
        host: MySqlConfig.host,
        user: MySqlConfig.user,
        password: MySqlConfig.password,
        database: MySqlConfig.database
    });
    // 创建连接
    connection.connect((err) => {
        if (err) {
            console.error('error connecting: ' + err.stack);
            console.log('<<<<<<<<请开启数据库>>>>>>>>>');
            return;
        }
    });
    // 进入执行主体
    connection.query(sqlorder, post, function (err, results, fields) {
        if (err) throw err;
        callback(err, results, fields, connection);
        // 断开连接
        connection.end();
    });
}
exports.mysql = mysql;