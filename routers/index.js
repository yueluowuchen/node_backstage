module.exports = function(app) {
  /**
   * 用户管理：用户注册、用户登录、忘记密码、实名认证、修改昵称
   */
  //用户注册
  const registered = require("../api/user/registered");
  app.use("/registered", registered);

  // 用户登录
  const login = require("../api/user/login");
  app.use("/login", login);

  //忘记密码
  const forget = require("../api/user/forget");
  app.use("/forget", forget);

  /**
   * 笔记操作
   * 笔记创建 笔记查询（公有查询、私有查询） 笔记收藏
   */

  //笔记创建
  const createnote = require("../api/note/createnote");
  app.use("/createnote", createnote);

  //私有笔记查询
  const searchSelfNote = require('../api/note/searchSelfNote');
  app.use("/searchSelfNote",searchSelfNote);

  // 公有笔记查询
  const searchCommonNote = require('../api/note/searchCommonNote');
  app.use("/searchCommonNote",searchCommonNote);

  // 查看笔记详情
  const viewnoteinfo = require('../api/note/viewnoteinfo');
  app.use('/viewnoteinfo',viewnoteinfo);
};
