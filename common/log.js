const MySql = require('../mysql/mysql')

exports.Log4j = (IP, RequestTime, RequestApi, Status, Message,RequestToken) => {
    /**
     * 接收客户端访问IP、请求时间、请求接口、服务器响应码、响应信息
     * 执行插入日志表
     */
    let post = {
        IP: IP,
        RequestTime: RequestTime,
        RequestApi: RequestApi,
        Status: Status,
        Message: Message,
        RequestToken:RequestToken
    }
    MySql.Mysql('INSERT INTO s_log SET ?', post, (err, results, fields) => {
        if (err) throw err;

        // 控制台打印日志
        let ipout = '[' + IP + ']';
        let timeout = '[' + RequestTime + ']';
        let apiout = '[' + RequestApi + ']';
        let statusout = '[' + Status + ']';
        let msgout = '[' + Message + ']';
        let requestToken = '[' + RequestToken + ']';
        console.log(ipout + ' ' + timeout + ' ' + apiout + ' ' + statusout + ' ' + msgout + ' '+ requestToken);
    })
}