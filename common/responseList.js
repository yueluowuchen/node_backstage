const Log4j = require("./log");

//定义后台返回客户端公共格式
exports.ResList = (req,res, status, message, sendData) => {
    let response = {
        status: status,
        msg: message,
        data: sendData
    }
    res.send(response);
}